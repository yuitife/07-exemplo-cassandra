package br.com.itau.pinterest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.pinterest.api.PerfilEntrada;
import br.com.itau.pinterest.models.Foto;
import br.com.itau.pinterest.services.FotoService;
import br.com.itau.pinterest.services.PerfilService;

@RestController
@RequestMapping("/foto")
public class FotoController {
	@Autowired
	FotoService fotoService;
	
	@GetMapping
	public Iterable<Foto> listar(){
		return fotoService.listarFotos();
	}
	
	@PostMapping
	public ResponseEntity criar(@RequestBody Foto foto) {
		fotoService.inserir(foto);
		
		return ResponseEntity.status(201).build();
	}
}
